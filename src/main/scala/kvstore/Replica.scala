package kvstore

import akka.actor._
import kvstore.Arbiter._
import kvstore.Replica.GetResult
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import scala.concurrent.duration._
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher


  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var idSeq = 0
  val system = akka.actor.ActorSystem("system")

  val persistor = context.system.actorOf(persistenceProps)
  arbiter ! Join

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  case class ActiveJob(var replicator:ActorRef, var key: String, var thread: Cancellable)
  import scala.language.postfixOps

  var invocations = Map.empty[Long, ActiveJob]


  val leader: Receive = {
    case Get(key, id) => get(key, id)
    case Insert(key, value, id)  => insert(key, value, id)
    case Remove(key, id)   =>   remove(key, id)
    case Replicated(key, id)   =>
    case Replicas(replicas)    => handleReplicas(replicas -self)
    case _=> println("Unexpected message type !")

  }


  val replica: Receive = {
    case Get(key, id) => get(key, id)
    case Snapshot(key, valueOption, seq) =>    handleSnapshot(key, valueOption, seq) // secondaries only
    case Persisted(key, id) =>{
      secondaryAcks get id match {
        case Some(x) =>{
          x.thread.cancel()
          secondaryAcks-=id
          x.replicator ! SnapshotAck(key, id)
        }
        case None =>
      }
    }
    case _=> println("Unexpected message type !")
  }


  def insert (key: String, value: String, id: Long): Unit = {
    //base condition check

    //simple insert
    kv +=  key -> value
    //response
    sender !  OperationAck(id)
  }


  def remove(key: String, id: Long){
    kv -= key
    sender !  OperationAck(id)
  }

  def get(key: String, id: Long): Unit = {
    sender ! GetResult(key, kv.get(key), id)
  }

  def handleReplicas(replicas: Set[ActorRef]): Unit = {

    // and when an existing replica leaves the cluster
    val reducedReplicas = secondaries.keySet.diff(replicas)

    // handle the case when a new replica added
    val newReplicas = replicas.diff(secondaries.keySet)

    for(replica <- reducedReplicas){
      val leavingReplica = secondaries(replica)
      leavingReplica ! PoisonPill
      replicators -= leavingReplica
      secondaries -= replica
    }


    for(replica <- newReplicas){
      val replicator = context.actorOf(Replicator.props(replica))
      replicators += replicator
      secondaries += ((replica, replicator))
      idSeq -= 1
      for(newReplicator <- replicators;
          (k,v) <- kv)  newReplicator ! Replicate(k, Option(v), idSeq)
    }


  }


  var sequence = 0l// set of unique ids for consistent updates
  var secondaryAcks = Map.empty[Long, ActiveJob]
  def handleSnapshot(key: String, valueOption: Option[String], seq: Long): Unit ={
    if(seq < sequence ){
      sender ! SnapshotAck(key, seq)
    } else if(sequence == seq){
      persistData(key, valueOption, seq)
      sequence += 1
    }
    else {
      println(" wrong sequence id")
    }
  }


  def persistData(key: String, valueOption: Option[String], seq:Long): Unit = {

      valueOption match {
        case Some(x) =>{

        kv += ((key, valueOption.get)) // insert
        secondaryAcks += seq ->ActiveJob(sender, key, context.system.scheduler.schedule(0 milliseconds, 100 microseconds, persistor, Persist(key, Some(x), seq) ))
        }
        case None => {

          kv -= key //remove
          secondaryAcks += seq ->ActiveJob(sender, key, context.system.scheduler.schedule(0 milliseconds, 100 microseconds, persistor, Persist(key, None, seq) ))
        }
      }
    }

}

